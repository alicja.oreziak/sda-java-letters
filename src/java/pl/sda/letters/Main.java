package pl.sda.letters;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String text = sc.nextLine();

        if (text.length() <= 3) {
            System.out.println((text.toUpperCase()));
        } else {
            String threeLastLetters = text.substring(text.length() - 3);
            String restLetters = text.substring(0, text.length() - 3);
            String textResult = restLetters + threeLastLetters.toUpperCase();

            System.out.println(textResult);
        }
    }
}
